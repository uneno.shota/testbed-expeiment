package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/net/websocket"
)

const (
	nodes_JSON_PATH = "nodes.json"
	SCHEMA_PATH     = "nict-client/schema/"
	MODE_LATENCY    = "latency"
	MODE_EVENTUAL   = "eventual"
	MODE_WORKLOAD   = "workload"
)

var (
	// user         string
	// password     string
	// programPath  string
	nodes        []*Node
	Vehicles     sync.Map
	roadSections map[string][]int = make(map[string][]int, 6)
	schemaNames  []string         = []string{"roadSection", "vehicle", "query", "district", "districtEvent"}
	monitorRoads []MonitorRoad    = []MonitorRoad{
		{Id: "000000000000000000000201", Edge: 1},
		{Id: "000000000000000000000102", Edge: 2},
		// {Id: "000000000000000000000210", Edge: 10},
	}
	startTimestamp string
	// 実験用
	intersectionCount     int
	numOfVehiclePerNodes  int
	turnSpanSecond        int
	turnProbability       int
	changeNodeSpanSecond  int
	changeNodeProbability int
	changeAreaSpanSecond  int
	waitSpanMilliSecond   int
	experimentDeadline    int
	movecount             int
	errorcount            int
	turnSpan              time.Duration
	changeNodeSpan        time.Duration
	changeAreaSpan        time.Duration
	waitSpan              time.Duration
	logfile               *os.File
	detailLog             = new(atomic.Bool)
	// ProgressWorkloadTestを終了させるための関数
	CancelProgressWorkloadTest context.CancelFunc
	timewebsocket              []int
)

type Node struct {
	Mu         sync.Mutex `json:"-"`
	Name       string     `json:"name"` // ログの視認性をあげるためのフィールド
	DbAddrPort string     `json:"db"`
	// DbAddrPort  netip.AddrPort `json:"target"`
	Mongo           string   `json:"mongo"`
	RoadSectionsIDs []string `json:"-"` // 管理下にあるroadSectionの_idがおかれる。
	Vehicles        sync.Map `json:"-"`
	// Session         *ssh.Session   `json:"-"`
	// StdIn  io.WriteCloser `json:"-"`
	// StdOut StdOutReader   `json:"-"`
	// StdErr StdOutReader   `json:"-"`
}

// 各道路で直進、右左折した場合の移動先が書かれる
type Directions struct {
	// 左折した場合に進入する道路のIDが書かれる
	Straight string `json:"straight"`
	// 左折した場合に進入する道路のIDが書かれる
	Left string `json:"left"`
	// 右折した場合に進入する道路のIDが書かれる
	Right string `json:"right"`
}

type MonitorRoad struct {
	Id   string
	Edge int
}

/*
TODO
Workloadテストのnode数が1,2の場合は未完成(クライアント側)。
必要に応じて作成すること。
*/

/* HTTPリクエスト系 */

type RequestType struct {
	Method       string         `json:Method`
	URL          string         `json:URL`
	Body         map[string]any `json:Body`
	ExpectedCode int            `json:"expectedCode"` // レスポンスのstatus codeを確認する際に用いる
	ExpectedBody string         `json:"expectedBody"` // リクエストのbodyを確認する際に用いる
}

type ResponseType struct {
	URL     string `json:URL`
	Code    int    `json:"code"`
	Body    string `json:Body`
	Elapsed int    `json:"elapsed"`
}

func main() {
	isInitiating := flag.Bool("init", false, "all nodes are not initiated")
	isReseting := flag.Bool("reset", false, "some nodes already have records")
	mode := flag.String("mode", MODE_WORKLOAD, "choose mode you execute")
	// numOfVehiclesInLatency := flag.Int("count-latency", 2, "number of vehicles in latency experiment")
	numOfCPU := flag.Int("cpu", -1, "number of cpu using in nodes")
	queryOption := flag.String("query-option", "", "set query option")
	flag.IntVar(&intersectionCount, "intersection", 2, "number of intersection")
	flag.Parse()

	// if *mode != MODE_LATENCY && *mode != MODE_EVENTUAL && *mode != MODE_WORKLOAD {
	// 	panic("invalid mode")
	// }

	ReadConfig()
	wg := sync.WaitGroup{}
	for _, node := range nodes {
		var err error
		// node.SSH()
		// defer client.CloseSession()

		// client.WriteStdIn("showLog")

		// ノードの初期設定が必要な場合はinitiate
		if *isInitiating {
			wg.Add(1)
			go func(n *Node) {
				err = n.Initiate()
				if err != nil {
					panic(err)
				}
				fmt.Println("Initiated ", n.Name)
				wg.Done()
			}(node)
		}

		if *isReseting {
			wg.Add(1)
			go func(n *Node) {
				err = n.ResetTarget()
				if err != nil {
					panic(err)
				}
				wg.Done()
			}(node)
		}
		if *numOfCPU > 0 {
			node.SetGOMAXPROCS(*numOfCPU)
		}
	}
	wg.Wait()

	startTimestamp = time.Now().String()
	// 実験を開始したことをログに打つ
	fmt.Println(("Start Experiment on " + startTimestamp))
	// // 実験を開始したことをログに打つ
	// for _, c := range nodes {
	// 	c.WriteLog("Start Experiment on " + startTimestamp)
	// }

	// // managerとclient間の遅延をチェック
	// for i := 0; i < 5; i++ {
	// 	var wg sync.WaitGroup
	// 	wg.Add(len(nodes))
	// 	for _, client := range nodes {
	// 		go func(client *Node) {
	// 			client.CheckTime()
	// 			wg.Done()
	// 		}(client)
	// 	}
	// 	wg.Wait()
	// }

	// //クライアントとノード間の遅延をチェック
	// for _, node := range nodes {
	// 	node.MeasureOtherNodesLatency()
	// }

	// // 通信遅延チェック
	// for _, node := range nodes {
	// 	for i := 0; i < 5; i++ {
	// 		node.GetSimply()
	// 	}
	// }

	for _, node := range nodes {
		// スキーマ登録
		wg.Add(1)
		go func(n *Node) {
			for _, name := range schemaNames {
				err := n.PostSchema(name)
				if err != nil {
					panic(err)
				}
			}
			wg.Done()
		}(node)
	}
	wg.Wait()
	// クエリ登録
	var err error
	if *mode == MODE_EVENTUAL {
		err = nodes[0].PostQuery("districtEventList", *queryOption)
	} else {
		err = nodes[0].PostQuery("vehicleList", *queryOption)
	}
	if err != nil {
		panic(err)
	}

	// mapとroadsectionを準備
	if *isInitiating {
		//map配置
		PutRoadSectionAndMap(intersectionCount * intersectionCount)
		// insert roadSections
		// err = PutRoadSections()
		// if err != nil {
		// 	panic(err)
		// }
	}

	switch *mode {
	case MODE_WORKLOAD:
		connectionClosers = make(map[string]context.CancelFunc)
		monitors = make(map[string]*MonitoringTarget)
		//ログを別ファイルに
		logfile, err = os.Create("latency-log.txt")
		if err != nil {
			fmt.Println(err)
			return
		}
		for _, monitorRoad := range monitorRoads {
			DialWebSocket(monitorRoad)
		}
		StartWorkloadTest()
		// 実験がおこなわれている間待機
		// time.Sleep(time.Duration(experimentDeadline+2) * time.Second)
		// 実験終了
		fmt.Println("Finish Experiment(Workload)")
		fmt.Println("movecount:", movecount, "errorcount:", errorcount, "errorrate:", float64(errorcount)/float64(movecount+errorcount)*100)
		sum := 0
		for _, time := range timewebsocket {
			sum += time
		}
		fmt.Println("average time:", sum/len(timewebsocket), "count:", len(timewebsocket))
	case "no":
		fmt.Println("no")
	}
}

// テスト環境について記載されたJSONを読み込む。
func ReadConfig() {
	raw, err := os.ReadFile(nodes_JSON_PATH)
	if err != nil {
		panic(err)
	}

	var contents struct {
		// User                  string    `json:"user"`
		// Password              string    `json:"password"`
		// Program               string    `json:"program"`
		Nodes                 []*Node `json:"nodes"`
		NumOfVehicles         int     `json:"numberOfVehiclesPerNodes"`
		TurnSpan              int     `json:"turnSpanSecond"`
		TurnProbability       int     `json:"turnProbability"`
		ChangeNodeSpan        int     `json:"changeNodeSpanSecond"`
		ChangeNodeProbability int     `json:"changeNodeProbability"`
		WaitSpanMilliSecond   int     `json:"waitSpanMilliSecond"`
		ExperimentDeadline    int     `json:"experimentDeadline"`
		ChangeAreaSpanSecond  int     `json:"changeAreaSpanSecond"`
	}

	err = json.Unmarshal(raw, &contents)
	if err != nil {
		panic(err)
	}

	// user = contents.User
	// password = contents.Password
	// programPath = contents.Program
	nodes = contents.Nodes
	numOfVehiclePerNodes = contents.NumOfVehicles
	turnSpanSecond = contents.TurnSpan
	turnProbability = contents.TurnProbability
	changeNodeSpanSecond = contents.ChangeNodeSpan
	changeNodeProbability = contents.ChangeNodeProbability
	waitSpanMilliSecond = contents.WaitSpanMilliSecond
	experimentDeadline = contents.ExperimentDeadline
	changeAreaSpanSecond = contents.ChangeAreaSpanSecond
}

/* リクエスト関連 */

func (n *Node) Initiate() error {
	fmt.Println("Initiate ", n.Name)
	body := map[string]any{
		"address": n.DbAddrPort,
		"mongo":   n.Mongo + ":27017",
	}
	reqtype := RequestType{
		Method: http.MethodPost,
		URL:    "http://" + n.DbAddrPort + "/",
		Body:   body,
	}

	// input := map[string]RequestType{
	// 	Method: http.MethodPost,
	// 	URL:    "http://" + n.DbAddrPort + "/",
	// 	Body:   body,
	// }
	// inputb, err := json.Marshal(input)
	// if err != nil {
	// 	return err
	// }
	// expected := "\"code\":" // status codeはなんでもいいので、とりあえずレスポンスがあることを確認
	n.SendRequest(reqtype)

	// fmt.Println("Expected \"", expected, "\" in ", n.Name)
	// n.WatchStdOut(expected, false)
	// fmt.Println("Initiated ", n.Name)
	return nil
}

/*
実験サーバ上からmongoのレコードとメモリ上のスキーマを削除する。
ログファイルの内容も削除する。
*/
func (n *Node) ResetTarget() error {
	input := RequestType{
		Method: http.MethodGet,
		URL:    "http://" + n.DbAddrPort + "/v2/log/reset/",
	}
	n.SendRequest(input)

	input = RequestType{
		Method: http.MethodDelete,
		URL:    "http://" + n.DbAddrPort + "/v2/manage/reset/",
	}
	n.SendRequest(input)
	return nil
}

func (n *Node) SetGOMAXPROCS(num int) {
	input := RequestType{
		Method: http.MethodPost,
		URL:    "http://" + n.DbAddrPort + "/v2/manage/change-procs/",
		Body:   map[string]any{"procs": num},
	}
	n.SendRequest(input)
}

func (n *Node) MeasureOtherNodesLatency() error {
	for _, node := range nodes {
		if n == node {
			continue
		}
		input := RequestType{
			Method: http.MethodPost,
			URL:    "http://" + n.DbAddrPort + "/v2/manage/measure-latency/",
			Body:   map[string]any{"addrport": node.DbAddrPort},
		}
		n.SendRequest(input)
	}
	return nil
}

/* シンプルなGetをおこなう。HTTPに通信遅延がどれくらいあるか確認するために用いる。 */
func (n *Node) GetSimply() error {
	// Clientの標準入力へ書き込み
	input := RequestType{
		Method: http.MethodGet,
		URL:    "http://" + n.DbAddrPort + "/v2/manage/status/",
	}
	n.SendRequest(input)
	return nil
}

/*
managerからclient間の遅延を確認する。
ついでに各clientでは各々のtimestampを表示させる。
*/
// func (c *Node) CheckTime() (latency int) {
// 	go c.WriteStdIn("checkTime")
// 	latency = c.WatchStdOut("time", true)
// 	return
// }

// func (c *Node) WriteLog(str string) {
// 	input := RequestType{
// 		Method: http.MethodPost,
// 		URL:    "http://" + c.DbAddrPort + "/v2/manage/write/",
// 		Body:   map[string]any{"message": str},
// 	}
// 	b, err := json.Marshal(input)
// 	if err != nil {
// 		panic(err)
// 	}
// 	go c.WriteStdIn("request", string(b))
// 	c.WatchStdOut(str, false) // リクエストが完了するまで待機
// }

func (n *Node) PostSchema(name string) error {
	fmt.Println("PostSchema ", n.Name, name)

	// スキーマファイル読み込み
	raw, err := os.ReadFile("./nict-client/schema/" + name + ".json")
	if err != nil {
		return err
	}

	// スキーマをパース
	var schema map[string]any
	err = json.Unmarshal(raw, &schema)
	if err != nil {
		return err
	}

	input := RequestType{
		Method: http.MethodPost,
		URL:    "http://" + n.DbAddrPort + "/v1/schema/" + name,
		Body:   schema,
	}
	n.SendRequest(input)
	return nil
}

func (n *Node) PostQuery(name, option string) error {
	fmt.Println("PostQuery ", n.Name, name)

	// クエリファイル読み込み
	raw, err := os.ReadFile("./nict-client/query/" + name + ".json")
	if err != nil {
		return err
	}

	// クエリをパース
	var query map[string]any
	err = json.Unmarshal(raw, &query)
	if err != nil {
		return err
	}

	switch option {
	case "sourceHomeOnly", "notEscapePropagation":
		query["options"] = map[string]any{option: true}
	}

	// coveredNodesを追加して、クエリを他ノードへ伝搬させる。
	coveredNodes := make(map[string]any, len(nodes))
	for _, node := range nodes {
		coveredNodes[node.Name] = node.DbAddrPort
	}
	query["coveredNodes"] = coveredNodes
	query["home"] = n.DbAddrPort

	// Clientの標準入力へ書き込み
	input := RequestType{
		Method: http.MethodPost,
		URL:    "http://" + n.DbAddrPort + "/v2/query/",
		Body:   query,
	}
	n.SendRequest(input)
	// TODO: 今のところ問題は起きていないが、stdoutを監視すべきかも

	return nil
}

/* Vehicle関連 */

type VehicleStatus struct {
	Mu          sync.Mutex `json:"-"`
	RoadSection string
	TurnStatus  string
	Home        string
}

// func (c *Node) PutVehicle(id uint64, roadSectionId uint64) error {
// 	fmt.Println("PutVehicle ", c.Name, id, roadSectionId)

// 	idstr := ID(id)
// 	roadSectionIdStr := ID(roadSectionId)
// 	v := map[string]any{
// 		"name": "vehicle" + strconv.FormatUint(id, 10),
// 		"home": c.DbAddrPort,
// 		"roadInfo": map[string]any{
// 			"roadSectionId": roadSectionIdStr,
// 			"turn":          "straight",
// 		},
// 		"_cache":   "centralizedOriginal",
// 		"_version": 0,
// 	}

// 	input := RequestType{
// 		Method: "PUT",
// 		URL:    "http://" + c.DbAddrPort + "/v1/service/vehicle/" + idstr,
// 		Body:   v,
// 	}

// 	b, err := json.Marshal(input)
// 	if err != nil {
// 		return err
// 	}
// 	go c.WriteStdIn("request", string(b))
// 	c.WatchStdOut(idstr, false)
// 	c.Vehicles.Store(idstr, *VehicleStatus{
// 		ID:          idstr,
// 		RoadSection: roadSectionIdStr,
// 		TurnStatus:  "straight",
// 	})

// 	return nil
// }

// func (c *Node) PatchVehicle(id string, change map[string]any) error {
// 	fmt.Println("PatchVehicle ", c.Name, id, change)

// 	input := RequestType{
// 		Method: http.MethodPatch,
// 		URL:    "http://" + c.DbAddrPort + "/v2/service/vehicle/" + id,
// 		Body:   change,
// 	}

// 	b, err := json.Marshal(input)
// 	if err != nil {
// 		return err
// 	}
// 	go c.WriteStdIn("request", string(b))
// 	l := c.WatchStdOut(id, false)
// 	if l == -1 {
// 		return fmt.Errorf(
// 			"vehicle %s is nothing in %s in processing PatchVehicle (%v)",
// 			id, c.Name, change)
// 	}

// 	return nil
// }

// 引数indexはnodesの配列番号で、VehicleレコードのIDやroadSectionMapに用いる。
func StartWorkloadTest() error {
	//変数代入
	turnSpan = time.Duration(turnSpanSecond) * time.Second
	changeNodeSpan = time.Duration(changeNodeSpanSecond) * time.Second
	waitSpan = time.Duration(waitSpanMilliSecond) * time.Millisecond
	changeAreaSpan = time.Duration(changeAreaSpanSecond) * time.Second

	type Edgeroad struct {
		// フィールド名と型を記述する
		List map[string][]string `json:"list"`
	}
	fmt.Println("start setup in StartWorkloadTest")
	//分散データストアからedgeとroadsectionの対応関係を取得

	edgeroad := new(Edgeroad)
	b, _, _, err := Request(http.MethodGet, "http://"+nodes[0].DbAddrPort+"/v1/service/edgeroad/"+ID(uint64(1)), nil)
	if err != nil {
		return fmt.Errorf("request error: %v", err)
	}
	err = json.Unmarshal(b, &edgeroad)
	if err != nil {
		panic(err)
	}
	//vehicle分配
	var wg sync.WaitGroup
	var mu sync.Mutex
	index := 0
	wg.Add(len(edgeroad.List))
	for edge, roads := range edgeroad.List {
		go func(edge string, roads []string) {
			mu.Lock()
			i := index
			index++
			mu.Unlock()
			baseurl := "http://" + edge + "/v1/service/vehicle/"
			rt := &RequestType{Method: http.MethodPut}
			for j := 0; j < numOfVehiclePerNodes; j++ {
				idint := numOfVehiclePerNodes*i + j
				id := ID(uint64(idint))
				rt.URL = baseurl + id
				rt.Body = map[string]any{
					"_id":  id,
					"name": "vehicle" + strconv.Itoa(idint),
					"home": edge,
					"roadInfo": map[string]any{
						"roadSectionId": roads[j%(len(roads))],
						"turn":          "straight",
					},
					"_cache":   "centralizedOriginal",
					"_version": 0,
				}
				// ここのmarshalはSendRequestでどうせUnmarshalするので無駄だが、
				// この辺りの処理は時間計測不要なので、とりあえず雑に書いている
				b, err := json.Marshal(rt)
				if err != nil {
					fmt.Fprintln(os.Stderr, err)
					return
				}
				err = SendRequest(string(b))
				if err != nil {
					fmt.Fprintln(os.Stderr, err)
					return
				}
				roadInfo, ok := rt.Body["roadInfo"].(map[string]any)
				if !ok {
					panic("roadInfo is not map[string]any")
				}
				rs, ok := roadInfo["roadSectionId"].(string)
				if !ok {
					panic("roadInfo.roadSectionId is not string")
				}
				tu, ok := roadInfo["turn"].(string)
				if !ok {
					panic("roadInfo.turn is not string")
				}
				Vehicles.Store(id, &VehicleStatus{
					RoadSection: rs,
					TurnStatus:  tu,
					Home:        edge,
				})
			}
			wg.Done()
		}(edge, roads)
	}
	wg.Wait()

	//デバッグ用
	// fmt.Println(Vehicles)
	count := 0
	Vehicles.Range(func(key, value interface{}) bool {
		count++
		return true
	})
	fmt.Println("要素数:", count)

	b, err = json.Marshal(map[string]any{"message": "StartWorkloadTest"})
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return err
	}

	for edge, _ := range edgeroad.List {
		_, code, _, err := Request(
			http.MethodPost,
			"http://"+edge+"/v2/manage/write/",
			bytes.NewBuffer(b),
		)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return err
		}
		if code != http.StatusCreated {
			fmt.Fprintln(os.Stderr, "invalid response code on WriteLog")
			return err
		}
	}

	ctx, cancel := context.WithCancel(context.TODO())
	CancelProgressWorkloadTest = cancel
	go ProgressWorkloadTest(ctx)
	fmt.Println("StartWorkloadTest is done")
	time.Sleep(time.Duration(experimentDeadline) * time.Second)
	cancel()
	return nil
}

func ProgressWorkloadTest(ctx context.Context) {
	movecount = 0
	errorcount = 0
	rand.Seed(time.Now().UnixNano())
	for {
		select {
		case <-ctx.Done():
			fmt.Println("Stopped ProgressWorkloadTest")
			return
		default:
			Vehicles.Range(func(key, value any) bool {
				id, ok := key.(string)
				if !ok {
					panic("invalid key in sync.Map, Vehicles")
				}
				vs, ok := value.(*VehicleStatus)
				if !ok {
					fmt.Printf("debug:%v(Type: %T)", value, value)
					panic("invalid value in sync.Map, Vehicles")
				}
				// if !vs.Mu.TryLock() { // もうすでにロックされている場合は、待たずに処理を止める
				// 	// fmt.Println("ロック済み")
				// 	return true
				// }
				Vehicles.Delete(id)
				var state string
				if r := rand.Intn(100); r < changeNodeProbability {
					state = "straight"
				} else if r < turnProbability+changeNodeProbability {
					if r%2 == 0 {
						state = "left"
					} else {
						state = "right"
					}
				} else {
					// WaitSpanだけ時間を置く
					go func(id string, vs *VehicleStatus) {
						time.Sleep(changeAreaSpan * 2)
						// vs.Mu.Unlock()
						// fmt.Println(id,"をアンロックしました")
						Vehicles.Store(id, &VehicleStatus{
							RoadSection: vs.RoadSection,
							TurnStatus:  vs.TurnStatus,
							Home:        vs.Home,
						})
						fmt.Println("store完了", id)
					}(id, vs)
					return true
				}
				go ChangeNode(id, state, vs)
				return true
			})
			// fmt.Println("要素数:", count)
		}
	}
}

func ChangeNode(id string, state string, vs *VehicleStatus) {
	// time.Sleep(ChangeNodeSpan) // ノード間移動するまで物理的な移動時間がかかるという雰囲気をここで作る
	var mu sync.Mutex
	type RoadSection struct {
		Direction    map[string]string `json:"direction"`
		CoveredNodes map[string]string `json:"coveredNodes"`
	}
	// fmt.Println("移動するよ")
	// fmt.Println(id, "test1")
	beforeroad := new(RoadSection)
	//vehicleが現在存在するroadsectionを取得
	b, code, _, err := Request(http.MethodGet, "http://"+vs.Home+"/v1/service/roadSection/"+vs.RoadSection, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Println("Request Error in ProgressWorkloadTest", err)
		return
	}
	if code != 200 {
		fmt.Fprintln(os.Stderr, "Unexpected response code in before_roadsection", string(b), beforeroad.Direction[state], vs.Home)
		return
	}
	err = json.Unmarshal(b, &beforeroad)
	if err != nil {
		panic(err)
	}
	// 行き先がnoである場合の処理
	direction := []string{"straight", "left", "right"}
	for {
		if beforeroad.Direction[state] == "999999999999999999999999" {
			direction = remove(direction, state)
			state = direction[0]
		} else {
			break
		}
	}
	//stateがstraightでない場合は、vehicleのstatusを変更
	vs.TurnStatus = state
	if detailLog.Load() {
		fmt.Printf("%s is turning %s on %s\n", id, state, vs.RoadSection)
	}
	//vehicleが移動するroadsectionを取得
	afterroad := new(RoadSection)
	b, code, _, err = Request(http.MethodGet, "http://"+vs.Home+"/v1/service/roadSection/"+beforeroad.Direction[state], nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Println("Request Error in ProgressWorkloadTest", err)
		return
	}
	if code != 200 {
		fmt.Fprintln(os.Stderr, "Unexpected response code in AfterEdge", string(b), beforeroad.Direction[state], vs.Home)
		return
	}
	err = json.Unmarshal(b, &afterroad)
	if err != nil {
		fmt.Println(string(b))
		panic(err)
	}
	//行先決定,status変更、Homeをbeforeedgeに変更
	if !(vs.Home == afterroad.CoveredNodes["beforeEdge"]) {
		fmt.Printf("Vehicle moving edge (beforeedge)")
		b, err = json.Marshal(map[string]any{
			"roadInfo": map[string]any{
				"roadSectionId": beforeroad.Direction[state],
				"turn":          state,
			},
			"home": afterroad.CoveredNodes["beforeEdge"],
		})
	} else {
		b, err = json.Marshal(map[string]any{
			"roadInfo": map[string]any{
				"roadSectionId": beforeroad.Direction[state],
				"turn":          state,
			},
		})
	}
	if err != nil {
		panic(err)
	}
	// 計測のための処理
	for _, monitorRoad := range monitorRoads {
		if monitorRoad.Id == beforeroad.Direction[state] {
			go SetMonitor(monitorRoad.Id, id)
		}
	}
	b, code, elapsed, err := Request(
		http.MethodPatch, "http://"+vs.Home+"/v2/service/vehicle/"+id, bytes.NewBuffer(b))
	if err != nil {
		fmt.Fprintln(os.Stderr, "Request Error in ProgressWorkloadTest", err)
		fmt.Println("Request Error in ProgressWorkloadTest", err)
		return
	}
	if code != 204 {
		fmt.Fprintln(os.Stderr, "Unexpected response code in ProgressWorkloadTest:changeRoadsection", code, id, vs.Home, string(b))
		// Vehicles.Delete(id)
		mu.Lock()
		errorcount++
		mu.Unlock()
		return
	}

	fmt.Printf("Vehicle %s is moving roadSection %s, time: %dμs\n", id, afterroad.CoveredNodes["beforeEdge"], elapsed)
	vs.RoadSection = beforeroad.Direction[state]
	vs.Home = afterroad.CoveredNodes["beforeEdge"]
	time.Sleep(changeAreaSpan) // 領域間移動するまで物理的な移動時間がかかるという雰囲気をここで作る
	// homeを設定
	b, err = json.Marshal(map[string]any{"home": afterroad.CoveredNodes["afterEdge"]})
	if err != nil {
		panic(err)
	}
	//request送信(homeをAfterEfgeに変更)
	b, code, elapsed, err = Request(
		http.MethodPatch,
		"http://"+vs.Home+"/v2/service/vehicle/"+id,
		bytes.NewBuffer(b))
	// fmt.Println(vs.Home, afterroad.AfterEdge, id, string(b), code, elapsed, err)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Request Error in ChangeNode", err)
		fmt.Println("Request Error in ProgressWorkloadTest", err)
	}
	if code != 204 {
		fmt.Println(string(b))
		return
	}
	fmt.Printf("Vehicle %s is moving from %s to %s (afteredge) on roadSection %s, time: %dμs\n", id, vs.Home, afterroad.CoveredNodes["afterEdge"], vs.RoadSection, elapsed)
	vs.Home = afterroad.CoveredNodes["afterEdge"]
	mu.Lock()
	movecount++
	mu.Unlock()
	time.Sleep(changeAreaSpan) // 交差点に移動するまで物理的な移動時間がかかるという雰囲気をここで作る
	// vs.Mu.Unlock()
	// fmt.Println("移動処理が完了したため",id,"をアンロックしました")
	Vehicles.Store(id, &VehicleStatus{
		RoadSection: vs.RoadSection,
		TurnStatus:  vs.TurnStatus,
		Home:        vs.Home,
	})
	// fmt.Println("changenodeでstore完了", id)
}

// func (c *Node) FinishWorkloadTest() {
// 	go c.WriteStdIn("finishWorkloadTest")
// 	c.WatchStdOut("Stopped ProgressWorkloadTest", true)
// }

// func TellMovingVehicle(message string) {
// 	id, addr, roadSectionID := ParseMovingVehicle(message)
// 	for _, c := range nodes {
// 		if addr != c.DbAddrPort {
// 			continue
// 		}
// 		input := map[string]any{
// 			"key": id,
// 			"value": map[string]any{
// 				"roadSection": roadSectionID,
// 				"turnStatus":  "straight",
// 			},
// 		}
// 		b, err := json.Marshal(input)
// 		if err != nil {
// 			panic(err)
// 		}

// 		go c.WriteStdIn("addVehicleOnWorkloadTest", string(b))
// 		c.WatchStdOut(fmt.Sprintf("Vehicle %s is accepted", id), false)
// 		return
// 	}
// }
/* WebSocket関連 */
type DialWebSocketInput struct {
	URL    string `json:"url"`
	Origin string `json:"origin"`
	Key    string `json:"key"`
}
type MonitoringTarget struct {
	Mutex   *sync.Mutex
	Targets []Target
}
type Target struct {
	expected string
	start    time.Time
}

// type MonitorRequest struct {
// 	Key    string `json:"key"`
// 	Expect string `json:"expect"`
// }

var (
	connectionClosers map[string]context.CancelFunc
	monitors          map[string]*MonitoringTarget
)

func DialWebSocket(mr MonitorRoad) error {
	var input DialWebSocketInput
	input.URL = "ws://" + nodes[mr.Edge].DbAddrPort + "/v1/ws/" + mr.Id
	input.Key = mr.Id
	input.Origin = "http://" + nodes[mr.Edge].DbAddrPort + "/ws"
	var conn *websocket.Conn
	var err error
	// dialに成功まで、3回繰り返し。3回繰り返すのは他コードに倣った。
	for i := 3; i > 0; i-- {
		conn, err = websocket.Dial(input.URL, "", input.Origin)
		if conn != nil && err == nil {
			break
		}
	}
	if err != nil {
		return err
	}
	fmt.Println("接続成功")
	ctx, cancel := context.WithCancel(context.Background())
	connectionClosers[input.Key] = cancel
	go ReceiveMessage(ctx, conn, input.Key)
	return nil
}

func ReceiveMessage(ctx context.Context, conn *websocket.Conn, key string) {
	/*
		この関数を呼び出す場合、例えば以下のようなctxを作成する
		ctx, cancel := context.WithCancel(context.Background())
	*/
	var msg string
	monitors[key] = new(MonitoringTarget)
	mt := monitors[key]
	mt.New()
	fmt.Println("start receiveMessage")
	for {
		select {
		case <-ctx.Done():
			err := conn.Close()
			if err != nil {
				fmt.Fprintln(os.Stderr, "Failed to close connection in ReceiveMessage of ", key, err)
			}
			fmt.Println("Finish ReceiveMessage of", key)
			return
		default:
			websocket.Message.Receive(conn, &msg)
			mt.Mutex.Lock()
			for i, t := range mt.Targets {
				if strings.Contains(msg, t.expected) {
					timewebsocket = append(timewebsocket, int(time.Since(t.start).Microseconds()))
					str := fmt.Sprintln(t.expected, "is found time:", int(time.Since(t.start).Microseconds()), "μs", key)
					fmt.Print(str)
					_, err := logfile.WriteString(str)
					if err != nil {
						fmt.Printf("[%d]Log Writer is failed.", err)
					}
					mt.Targets = append(mt.Targets[:i], mt.Targets[i+1:]...)
					break
				}
			}
			mt.Mutex.Unlock()
			// fmt.Println("ReceiveMessage", key, msg)
		}
	}
}

func SetMonitor(roadsectionid string, vehicleid string) error {
	// var mr MonitorRequest
	// mr.Key = roadsectionid
	// mr.Expect = vehicleid
	monitors[roadsectionid].Set(vehicleid)
	return nil
}

func (mt *MonitoringTarget) New() {
	mt.Mutex = new(sync.Mutex)
	// 監視対象文字列を格納する。とりあえず10メモリ分確保
	mt.Targets = make([]Target, 0, 10)
}

func (mt *MonitoringTarget) Set(target string) {
	mt.Mutex.Lock()
	mt.Targets = append(mt.Targets, Target{target, time.Now()})
	mt.Mutex.Unlock()
}

/* district関連 */

// func (c *Node) PutDistrict(id uint64, coveringNodes map[string]string) error {
// 	fmt.Println("PutDistrict ", c.Name, id)

// 	name := "district" + strconv.FormatUint(id, 10)
// 	bm := map[string]any{
// 		"name":          name,
// 		"home":          c.DbAddrPort,
// 		"coveringNodes": coveringNodes,
// 		"districtEvent": ID(id),
// 		"_cache":        "centralizedOriginal",
// 		"_version":      0,
// 	}

// 	input := RequestType{
// 		Method: http.MethodPut,
// 		URL:    "http://" + c.DbAddrPort + "/v1/service/district/" + ID(id),
// 		Body:   bm,
// 	}

// 	b, err := json.Marshal(input)
// 	if err != nil {
// 		return err
// 	}
// 	go c.WriteStdIn("request", string(b))

// 	c.WatchStdOut(name, false)

// 	fmt.Println("PutDistrictEvent ", c.Name, id)

// 	name = "districtEvent" + strconv.FormatUint(id, 10)
// 	bm = map[string]any{
// 		"name":          name,
// 		"home":          c.DbAddrPort,
// 		"coveringNodes": coveringNodes,
// 		"_cache":        "centralizedOriginal",
// 		"_version":      0,
// 	}

// 	input = RequestType{
// 		Method: http.MethodPut,
// 		URL:    "http://" + c.DbAddrPort + "/v1/service/districtEvent/" + ID(id),
// 		Body:   bm,
// 	}

// 	b, err = json.Marshal(input)
// 	if err != nil {
// 		return err
// 	}
// 	go c.WriteStdIn("request", string(b))

// 	c.WatchStdOut(name, false)

// 	return nil
// }

func (n *Node) SendRequest(reqtype RequestType) error {
	// var reqtype RequestType
	// err := json.Unmarshal([]byte(input), &reqtype)
	// if err != nil {
	// 	return fmt.Errorf("json.Unmarshal error: %v", err)
	// }
	body, err := json.Marshal(reqtype.Body)
	if err != nil {
		return fmt.Errorf("json.Marshal error for request body: %v", err)
	}

	b, code, elapsed, err := Request(reqtype.Method, reqtype.URL,
		bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("request error: %v", err)
	}
	respbody := string(b)

	if reqtype.ExpectedCode != 0 && code != reqtype.ExpectedCode {
		fmt.Fprintf(os.Stderr, "response code %d is not expected\nrequest: %v\n",
			code, reqtype)
	}
	if reqtype.ExpectedBody != "" && !strings.Contains(respbody, reqtype.ExpectedBody) {
		fmt.Fprintf(os.Stderr, "response body %s is not expected.\nrequest: %v\n",
			respbody, reqtype)
	}

	restype := ResponseType{
		URL: reqtype.URL, Code: code, Body: string(b), Elapsed: elapsed}
	b, err = json.Marshal(restype)
	if err != nil {
		return fmt.Errorf("json.Marshal error for response: %v", err)
	}
	fmt.Println(string(b))
	return nil
}

func Request(method, url string, body io.Reader) ([]byte, int, int, error) {
	var req *http.Request
	var err error
	if method == http.MethodGet {
		req, err = http.NewRequest(method, url, nil)
	} else {
		req, err = http.NewRequest(method, url, body)
	}
	if err != nil {
		return nil, 0, 0, fmt.Errorf("failed to do http.NewRequest: %v", err)
	}

	c := &http.Client{}
	start := time.Now()
	resp, err := c.Do(req)
	if err != nil {
		return nil, 0, 0, fmt.Errorf("failed to request: %v", err)
	}
	defer resp.Body.Close()
	elapsed := int(time.Since(start).Microseconds())

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, 0, fmt.Errorf("failed to read body: %v", err)
	}

	return b, resp.StatusCode, elapsed, nil
}

//roadsectionのmapを作成、配置
func PutRoadSectionAndMap(n int) {
	var roadid string
	//対応関係表
	edgeroad := make(map[string][]string)
	for i := 1; i <= n; i++ {
		//i→i+1の処理
		if i < n && i%intersectionCount != 0 {
			roadid = fmt.Sprintf("%022d", i) + fmt.Sprintf("%02d", i+1)
			PutRoadSection(GoStraightRight, roadid, i, i+1)
			edgeroad[nodes[i+1].DbAddrPort] = append(edgeroad[nodes[i+1].DbAddrPort], roadid)
		}
		//i→i-1の処理
		if i-1 > 0 && i%intersectionCount != 1 {
			roadid = fmt.Sprintf("%022d", i) + fmt.Sprintf("%02d", i-1)
			PutRoadSection(GoStraightLeft, roadid, i, i-1)
			edgeroad[nodes[i-1].DbAddrPort] = append(edgeroad[nodes[i-1].DbAddrPort], roadid)
		}
		//i→i-4の処理
		if i-intersectionCount > 0 {
			roadid = fmt.Sprintf("%022d", i) + fmt.Sprintf("%02d", i-intersectionCount)
			PutRoadSection(GoStraightUp, roadid, i, i-intersectionCount)
			edgeroad[nodes[i-intersectionCount].DbAddrPort] = append(edgeroad[nodes[i-intersectionCount].DbAddrPort], roadid)
		}
		//i→i+4の処理
		if i+intersectionCount <= n {
			roadid = fmt.Sprintf("%022d", i) + fmt.Sprintf("%02d", i+intersectionCount)
			PutRoadSection(GoStraightDown, roadid, i, i+intersectionCount)
			edgeroad[nodes[i+intersectionCount].DbAddrPort] = append(edgeroad[nodes[i+intersectionCount].DbAddrPort], roadid)
		}
	}
	//対応表の登録
	er := map[string]any{
		"name": "edgeroad",
		"list": edgeroad,
	}
	fmt.Println(edgeroad)
	input := RequestType{
		Method: http.MethodPut,
		URL:    "http://" + nodes[0].DbAddrPort + "/v1/service/edgeroad/" + ID(uint64(1)),
		Body:   er,
	}
	nodes[0].SendRequest(input)
}

func PutRoadSection(fn func(int) Directions, id string, before int, after int) {
	rs := make(map[string]any)
	rs["_id"] = id
	rs["_cache"] = "centralizedOriginal"
	rs["_version"] = 0
	rs["home"] = nodes[after].DbAddrPort
	rs["name"] = "roadSection" + strings.ReplaceAll(fmt.Sprint(before, after), " ", "")
	rs["direction"] = fn(before)
	rs["coveredNodes"] = map[string]any{
		"beforeEdge": nodes[before].DbAddrPort,
		"afterEdge":  nodes[after].DbAddrPort,
	}
	input := RequestType{
		Method: http.MethodPut,
		URL:    "http://" + nodes[after].DbAddrPort + "/v1/service/roadSection/" + id,
		Body:   rs,
	}
	nodes[after].SendRequest(input)

}

func GoStraightRight(i int) Directions {
	var directions Directions
	if i <= intersectionCount {
		directions.Left = "999999999999999999999999"
	} else {
		directions.Left = fmt.Sprintf("%022d", i+1) + fmt.Sprintf("%02d", i-intersectionCount+1)
	}
	if i >= intersectionCount*intersectionCount-intersectionCount+1 {
		directions.Right = "999999999999999999999999"
	} else {
		directions.Right = fmt.Sprintf("%022d", i+1) + fmt.Sprintf("%02d", i+intersectionCount+1)
	}
	if (i+1)%intersectionCount == 0 {
		directions.Straight = "999999999999999999999999"
	} else {
		directions.Straight = fmt.Sprintf("%022d", i+1) + fmt.Sprintf("%02d", i+2)
	}
	return directions
}

func GoStraightLeft(i int) Directions {
	var directions Directions
	if i <= intersectionCount {
		directions.Right = "999999999999999999999999"
	} else {
		directions.Right = fmt.Sprintf("%022d", i-1) + fmt.Sprintf("%02d", i-intersectionCount-1)
	}
	if i >= intersectionCount*intersectionCount-intersectionCount+1 {
		directions.Left = "999999999999999999999999"
	} else {
		directions.Left = fmt.Sprintf("%022d", i-1) + fmt.Sprintf("%02d", i+intersectionCount-1)
	}
	if (i-1)%intersectionCount == 1 {
		directions.Straight = "999999999999999999999999"
	} else {
		directions.Straight = fmt.Sprintf("%022d", i-1) + fmt.Sprintf("%02d", i-2)
	}
	return directions
}

func GoStraightUp(i int) Directions {
	var directions Directions
	if i%intersectionCount == 0 {
		directions.Right = "999999999999999999999999"
	} else {
		directions.Right = fmt.Sprintf("%022d", i-intersectionCount) + fmt.Sprintf("%02d", i-intersectionCount+1)
	}
	if i%intersectionCount == 1 {
		directions.Left = "999999999999999999999999"
	} else {
		directions.Left = fmt.Sprintf("%022d", i-intersectionCount) + fmt.Sprintf("%02d", i-intersectionCount-1)
	}
	if i <= intersectionCount*2 {
		directions.Straight = "999999999999999999999999"
	} else {
		directions.Straight = fmt.Sprintf("%022d", i-intersectionCount) + fmt.Sprintf("%02d", i-intersectionCount*2)
	}
	return directions
}

func GoStraightDown(i int) Directions {
	var directions Directions
	if i%intersectionCount == 0 {
		directions.Left = "999999999999999999999999"
	} else {
		directions.Left = fmt.Sprintf("%022d", i+intersectionCount) + fmt.Sprintf("%02d", i+intersectionCount+1)
	}
	if i%intersectionCount == 1 {
		directions.Right = "999999999999999999999999"
	} else {
		directions.Right = fmt.Sprintf("%022d", i+intersectionCount) + fmt.Sprintf("%02d", i+intersectionCount-1)
	}
	if i >= intersectionCount*intersectionCount-intersectionCount*2+1 {
		directions.Straight = "999999999999999999999999"
	} else {
		directions.Straight = fmt.Sprintf("%022d", i+intersectionCount) + fmt.Sprintf("%02d", i+intersectionCount*2)
	}
	return directions
}

func SendRequest(input string) error {
	var reqtype RequestType
	err := json.Unmarshal([]byte(input), &reqtype)
	if err != nil {
		return fmt.Errorf("json.Unmarshal error: %v", err)
	}
	body, err := json.Marshal(reqtype.Body)
	if err != nil {
		return fmt.Errorf("json.Marshal error for request body: %v", err)
	}

	b, code, elapsed, err := Request(reqtype.Method, reqtype.URL,
		bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("request error: %v", err)
	}
	respbody := string(b)

	if reqtype.ExpectedCode != 0 && code != reqtype.ExpectedCode {
		fmt.Fprintf(os.Stderr, "response code %d is not expected\nrequest: %v\n",
			code, reqtype)
	}
	if reqtype.ExpectedBody != "" && !strings.Contains(respbody, reqtype.ExpectedBody) {
		fmt.Fprintf(os.Stderr, "response body %s is not expected.\nrequest: %v\n",
			respbody, reqtype)
	}

	restype := ResponseType{
		URL: reqtype.URL, Code: code, Body: string(b), Elapsed: elapsed}
	b, err = json.Marshal(restype)
	if err != nil {
		return fmt.Errorf("json.Marshal error for response: %v", err)
	}
	fmt.Println(string(b))
	return nil
}

/* utilities */
func ID(decimal uint64) string {
	return fmt.Sprintf("%024x", decimal)
}

func remove(strings []string, search string) []string {
	result := []string{}
	for _, v := range strings {
		if v != search {
			result = append(result, v)
		}
	}
	return result
}
